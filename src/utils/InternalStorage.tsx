/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import AsyncStorage from '@react-native-community/async-storage';

// eslint-disable-next-line consistent-return
export const storeData = async (key: string, value: any) => {
  try {
    return await AsyncStorage.setItem(key, JSON.stringify(value));
  } catch (error) {
    console.info('Async storage error', error);
  }
};

// eslint-disable-next-line consistent-return
export const getData = async (key: string) => {
  try {
    const data = await AsyncStorage.getItem(key);
    if (data !== null) {
      return JSON.parse(data);
    }
    return data;
  } catch (error) {
    console.info('Async storage error', error);
  }
};

// eslint-disable-next-line consistent-return
export const clearData = async (key: string) => {
  try {
    return await AsyncStorage.removeItem(key);
  } catch (error) {
    console.info('Async storage error', error);
  }
};
// eslint-disable-next-line consistent-return
export const clearAll = async () => {
  try {
    return await AsyncStorage.clear();
  } catch (error) {
    console.info('Async storage error', error);
  }
};
