/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-disable react/no-array-index-key */
import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  TouchableNativeFeedback,
  TouchableOpacity,
  Text,
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import Typography from '../typography/Typography';
import {colors} from '../../styles/colors';
import usePrevious from '../../hooks/usePrevious';

interface ButtonProps {
  title: string;
  onClick: () => void;
}

interface LongButtonWithAnimationProps {
  buttons: ButtonProps[];
}

const LongButtonWithAnimation = ({buttons}: LongButtonWithAnimationProps) => {
  const [buttonIndex, setButtonIndex] = useState(0);

  const previousButtonIndex = usePrevious(buttonIndex);
  const fadeInLeftView = (index: number) => {
    if (index === buttonIndex) {
      return {
        from: {
          backgroundColor: colors.lightGrey,
        },
        to: {
          backgroundColor: colors.brand_color,
        },
      };
    } else {
      if (index === previousButtonIndex) {
        return {
          from: {
            backgroundColor: colors.brand_color,
          },
          to: {
            backgroundColor: colors.lightGrey,
          },
        };
      }
      return {
        from: {
          backgroundColor: colors.lightGrey,
        },
        to: {
          backgroundColor: colors.lightGrey,
        },
      };
    }
  };
  return (
    <View style={styles.container}>
      {buttons.map((el, index) => (
        <TouchableOpacity
          style={{
            backgroundColor: 'transparent',
            justifyContent: 'center',
            flex: 1,
            alignItems: 'stretch',
            height: 35,
          }}
          onPress={() => {
            el.onClick();
            setButtonIndex(index);
          }}
          key={el.title}>
          <Animatable.View
            animation={fadeInLeftView(index)}
            duration={300}
            style={[styles.defaultButton, {flex: 1, justifyContent: 'center'}]}>
            <Text style={{textAlign: 'center'}}>
              <Typography
                variant="button"
                color={
                  index === buttonIndex ? 'brand_content_on_brand' : 'link'
                }>
                {el.title.toUpperCase()}
              </Typography>
            </Text>
          </Animatable.View>
        </TouchableOpacity>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: colors.lightGrey,
    borderRadius: 10,
    flexDirection: 'row',
    padding: 0,
    flex: 1,
    height: 35,
  },
  defaultButton: {
    borderRadius: 10,
    flex: 1,
  },
});

export default LongButtonWithAnimation;
