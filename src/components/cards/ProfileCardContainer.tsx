/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */
import React, {useState} from 'react';
import {View, StyleSheet, Image} from 'react-native';
import {Avatar} from 'react-native-elements';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import CardRow from './CardRow';
import Typography from '../typography/Typography';
import {colors} from '../../styles/colors';
import LongButtonWithAnimation from '../button/LongButtonWithAnimation';
import CheckButton from '../button/CheckButton';
import {Effects} from '../../styles';

const ProfileCardContainer = () => {
  const [isVisible, setIsVisible] = useState(false);

  return (
    <View style={styles.container}>
      <CardRow styles={{alignItems: 'center'}}>
        <View style={{position: 'relative'}}>
          <Avatar
            size={100}
            rounded
            source={require('../../../assets/images/avatar_default.png')}
          />
          <View style={{position: 'absolute', right: 0}}>
            <Image
              source={require('../../../assets/images/statusIcon.png')}
              style={{height: 30, width: 30, resizeMode: 'cover'}}
            />
          </View>
        </View>
        <View style={{marginLeft: 30, flex: 1}}>
          <Typography variant="h1" bold>
            Anonymous
          </Typography>
        </View>
      </CardRow>
      <View style={{marginVertical: 10}}>
        <LongButtonWithAnimation
          buttons={[
            {title: 'away', onClick: () => {}},
            {title: 'free', onClick: () => {}},
            {title: 'busy', onClick: () => {}},
          ]}
        />
      </View>
      <CardRow
        styles={{
          marginVertical: 10,
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <CardRow styles={{alignItems: 'center', marginRight: 15}}>
          <Icon name="map-marker" size={20} color={colors.grey} />
          <View style={{marginLeft: 10}}>
            <Typography variant="body1">D-wing, T-building</Typography>
          </View>
        </CardRow>
        <View style={{flex: 1}}>
          <CheckButton
            buttonText="visible to others"
            onPress={() => setIsVisible(!isVisible)}
            isSelected={isVisible}
            isWhiteText={false}
          />
        </View>
      </CardRow>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginVertical: 10,
    backgroundColor: colors.card_background,
    borderRadius: 5,
    ...Effects.shadow,
  },
  marginText: {
    marginTop: 10,
  },
});

export default ProfileCardContainer;
