/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as React from 'react';
import {Animated, StyleSheet, View, ViewStyle} from 'react-native';
import {Effects} from '../../styles';
import {purpleLight, white} from '../../styles/colors';

export const Card: React.FC<CardProps> = ({
  children,
  cardStyles = {},
  externalStyles = {},
}) => (
  <Animated.View style={externalStyles}>
    <View style={[styles.card, cardStyles]}>{children}</View>
  </Animated.View>
);

export interface CardProps {
  children?: JSX.Element[] | JSX.Element;
  cardStyles?: ViewStyle;
  externalStyles?: any;
}

const styles = StyleSheet.create({
  card: {
    minWidth: 300,
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginVertical: 10,
    backgroundColor: white,
    borderRadius: 5,
    borderLeftColor: purpleLight,
    borderLeftWidth: 3,
    ...Effects.shadow,
  },
});
