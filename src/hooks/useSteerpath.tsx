/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {useEffect, useState} from 'react';
import {storeData, getData, clearData} from '../utils/InternalStorage';
import RNFS from 'react-native-fs';
import {SmartMapManager} from 'react-native-steerpath-smart-map';

export interface UserState {
  userId: string;
  title: string;
  profileImageUrl: string;
  userStatus: string;
  //available: boolean;
}

const baseurl =
  'https://2jcw71uk36.execute-api.eu-west-1.amazonaws.com/prod/configs?code=';

const CONFIG_FILE_PATH = RNFS.DocumentDirectoryPath + '/steerpath_config.json';

export function useSteerpath() {
  const [initializing, setInitializing] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [errorMsg, setErrorMsg] = useState<string>();
  const [sdkReady, setSdkReady] = useState(false);
  const [userData, setUserData] = useState<UserState | {}>({});

  useEffect(() => {
    getStoredVenueData();
  }, []);

  async function fetchVenueData(venueCode: string) {
    setIsLoading(true);
    const url = baseurl + venueCode;
    fetch(url)
      .then(response => response.json())
      .then(responseJson => {
        const venueData = responseJson.item;

        if (venueData) {
          storeData('venueData', venueData);
          startSteerpath(venueData);
        } else if (responseJson.err) {
          let errorMsg = responseJson.err;
          if (errorMsg === 'Not Found') {
            errorMsg = 'Venue not found with given code.';
          }
          setErrorMsg(errorMsg);
          setIsLoading(false);
        }
      })
      .catch(error => {
        let errorMsg;
        if (error && error.message) {
          errorMsg = error.message;
        }
        setIsLoading(false);
        setErrorMsg(errorMsg);
      });
  }

  async function getStoredVenueData() {
    const venueData = await getData('venueData');
    if (venueData) {
      startSteerpath(venueData);
    } else {
      setInitializing(false);
    }
  }

  async function getStoredUserData() {
    try {
      const storedUser = await getData('user');
      if (storedUser) {
        setUserData(storedUser);
      }
    } catch (err) {
      console.info('Async Storage Error', err);
    }
  }

  function startSteerpath(venueData) {
    RNFS.writeFile(CONFIG_FILE_PATH, venueData.config, 'utf8')
      .then(() => {
        // TODO: if user data, start location sharing / SP Live
        SmartMapManager.startWithConfig({
          apiKey: venueData.smartAPIKey,
          configFilePath: CONFIG_FILE_PATH,
        });
        console.log('FILE WRITTEN!');
        setSdkReady(true);
        setIsLoading(false);
        setInitializing(false);
      })
      .catch(err => {
        console.log(err.message);
      });
  }

  function clearSavedData() {
    RNFS.unlink(CONFIG_FILE_PATH)
      .then(() => {
        console.log('FILE DELETED');
      })
      // `unlink` will throw an error, if the item to unlink does not exist
      .catch(err => {
        console.log(err.message);
      });

    clearData('venueData');
    setSdkReady(false);
  }

  useEffect(() => {
    // TODO: start live
  }, [userData]);

  return {
    initializing,
    isLoading,
    errorMsg,
    setErrorMsg,
    fetchVenueData,
    sdkReady,
    clearSavedData,
  };
}
