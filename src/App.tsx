/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import MapScreen from './screens/MapScreen';
import SetCodeScreen from './screens/SetCodeScreen';
import SplashScreen from 'react-native-splash-screen';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import OptionsScreen from './screens/OptionsScreen';
import {colors} from './styles/colors';
import {View} from 'react-native';
import {typography} from './styles/typography';
import AboutScreen from './screens/AboutScreen';
import {SteerpathContext} from './context/SteerpathContext';
import {useSteerpath} from './hooks/useSteerpath';
import Spinner from './components/Spinner';
import LocationSharingScreen from './screens/LocationSharingScreen';
import {
  SafeAreaProvider,
  SafeAreaConsumer,
} from 'react-native-safe-area-context';

const AppStack = createStackNavigator();
const OptionStack = createStackNavigator();
const Tabs = createBottomTabNavigator();

export default function App() {
  const {
    initializing,
    isLoading,
    fetchVenueData,
    sdkReady,
    clearSavedData,
    errorMsg,
    setErrorMsg,
  } = useSteerpath();

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  function MapStack() {
    return (
      <Tabs.Navigator
        initialRouteName="Map"
        tabBarOptions={{
          indicatorStyle: {
            borderTopWidth: 3,
            borderTopColor: colors.brand_color,
          },
          activeTintColor: colors.brand_color,
          inactiveTintColor: colors.brand_color,
          showLabel: true,
          labelStyle: {
            fontSize: typography.body1.fontSize,
          },
          style: {
            backgroundColor: colors.card_background,
            height: 65,
            elevation: 5,
            borderTopWidth: 0,
            paddingBottom: 0,
          },
        }}
        screenOptions={({route}) => ({
          tabBarIcon: ({focused}) => {
            let iconName;
            switch (route.name) {
              case 'Map':
                iconName = 'map';
                break;
              case 'Options':
                iconName = 'cogs';
                break;
            }

            // You can return any component that you like here!
            return (
              <View
                style={
                  [
                    {
                      width: '100%',
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                      paddingTop: 10,
                    },
                    focused
                      ? {borderTopColor: colors.brand_color, borderTopWidth: 2}
                      : {},
                  ] as any
                }>
                <Icon name={iconName} size={26} color={colors.brand_color} />
              </View>
            );
          },
        })}>
        <Tabs.Screen name="Map" component={MapScreen} />
        <Tabs.Screen name="Options" component={OptionsStack} />
      </Tabs.Navigator>
    );
  }

  function OptionsStack() {
    return (
      <OptionStack.Navigator
        initialRouteName="Options"
        screenOptions={{
          headerTintColor: colors.white,
          headerStyle: {
            backgroundColor: colors.brand_color,
          },
          headerTitleStyle: {
            color: colors.white,
            alignSelf: 'center',
            textAlign: 'center',
            justifyContent: 'center',
            flex: 1,
            textAlignVertical: 'center',
          },
        }}>
        <OptionStack.Screen
          name="Options"
          component={OptionsScreen}
          options={{
            headerShown: false,
          }}
        />
        <OptionStack.Screen name="About" component={AboutScreen} />
        <OptionStack.Screen
          name="Share location"
          component={LocationSharingScreen}
        />
      </OptionStack.Navigator>
    );
  }

  return (
    <SafeAreaProvider>
      <SteerpathContext.Provider
        value={{
          initializing,
          isLoading,
          fetchVenueData,
          sdkReady,
          clearSavedData,
          errorMsg,
          setErrorMsg,
        }}>
        <SafeAreaConsumer>
          {insets => (
            <View style={{flex: 1, marginBottom: insets ? insets.bottom : 0}}>
              <NavigationContainer>
                {!initializing ? (
                  <AppStack.Navigator
                    initialRouteName="MapStack"
                    headerMode="none">
                    {sdkReady ? (
                      <AppStack.Screen name="Map" component={MapStack} />
                    ) : (
                      <AppStack.Screen
                        name="Set Code"
                        component={SetCodeScreen}
                      />
                    )}
                  </AppStack.Navigator>
                ) : (
                  <Spinner />
                )}
              </NavigationContainer>
            </View>
          )}
        </SafeAreaConsumer>
      </SteerpathContext.Provider>
    </SafeAreaProvider>
  );
}
