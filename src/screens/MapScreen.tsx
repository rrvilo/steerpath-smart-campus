/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useRef, useState, useEffect} from 'react';
import {SmartMapView} from 'react-native-steerpath-smart-map';
import {View, Platform, BackHandler} from 'react-native';
import ColoredStatusBar from '../components/status-bar/ColoredStatusBar';

export default function MapScreen({navigation}) {
  const smartMapRef = useRef<any>(null);
  const [isMapScreenVisible, setMapScreenVisible] = useState(false);

  const onMapLoaded = () => {
    setMapScreenVisible(true);

    /**
     * You should consider adding the subsrciptions if your application has more than one screen. This is tested
     * with TabNavigator only. Without calling stop() and start() methods of SmartMap, the app may freeze when
     * navigating away from the MapScreen.
     */
    if (Platform.OS === 'android') {
      const willBlurSubscription = navigation.addListener('blur', _ => {
        smartMapRef.current.stop();
        setMapScreenVisible(false);
      });

      const willFocusSubscription = navigation.addListener('focus', _ => {
        smartMapRef.current.start();
        setMapScreenVisible(true);
      });
    }
  };

  const onMapClicked = payload => {
    const mapObjects = payload.mapObjects;
    if (mapObjects.length > 0) {
      const mapObject = mapObjects[0];
      smartMapRef.current.selectMapObject(mapObject);
    }
  };

  /**
   * If you want to use Android native back button to handle different states of
   * the search bottomsheet of the smart map, you should add 'hardwareBackPress' -listener
   * to your Map screen.
   */
  useEffect(() => {
    let backHandlerListener;
    if (Platform.OS === 'android') {
      if (isMapScreenVisible) {
        backHandlerListener = BackHandler.addEventListener(
          'hardwareBackPress',
          androidBackPressedListener,
        );
      }
    }
    return () => {
      backHandlerListener && backHandlerListener.remove();
    };
  }, [isMapScreenVisible]);

  const androidBackPressedListener = async () => {
    if (Platform.OS === 'android') {
      const resp = await new Promise((res, rej) => {
        smartMapRef.current.onBackPressed(callback => {
          if (!callback) {
            // TODO: add dialog for confirming the exit
            navigation.navigate('Set Code');
            res(false);
          }
          res(true);
        });
      });
      return resp;
    }
  };

  return (
    <>
      <ColoredStatusBar />
      <View style={{flex: 1, position: 'relative'}}>
        <SmartMapView
          style={{flex: 1}}
          ref={smartMapRef}
          onMapLoaded={onMapLoaded}
          onMapClicked={onMapClicked}
          onSearchResultSelected={mapObject => {
            smartMapRef.current.selectMapObject(mapObject);
          }}
        />
      </View>
    </>
  );
}
