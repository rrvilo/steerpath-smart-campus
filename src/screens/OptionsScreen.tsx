/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useRef} from 'react';
import {StyleSheet, View, ScrollView} from 'react-native';
import {PreferenceList} from '../components/preferences/PreferenceList';
import {useSteerpathContext} from '../context/SteerpathContext';
import {PreferenceListItem} from '../components/preferences/PreferenceItem';
import {baseFontSize} from '../styles/typography';
import {colors, darkGray} from '../styles/colors';
import Text from '../components/typography/Typography';
import ProfileCardContainer from '../components/cards/ProfileCardContainer';
import ColoredStatusBar from '../components/status-bar/ColoredStatusBar';

export default function OptionsScreen({navigation}) {
  const {clearSavedData} = useSteerpathContext();
  const profileHeightRef = useRef(0);

  return (
    <>
      <ColoredStatusBar />
      <ScrollView style={styles.profileContainer} bounces={false}>
        <View style={styles.profileHeader}>
          <View
            style={{
              height: 60,
              width: '100%',
              flexDirection: 'row',
              alignItems: 'center',
              paddingHorizontal: 10,
              justifyContent: 'space-between',
              backgroundColor: colors.brand_color,
            }}>
            <View style={{flex: 1, alignItems: 'center'}}>
              <Text bold variant="h3" color="brand_content_on_brand">
                Options
              </Text>
            </View>
          </View>
        </View>
        <View
          style={{
            backgroundColor: colors.brand_color,
            width: '100%',
            height: profileHeightRef.current / 2,
          }}
        />
        <View
          style={{
            marginTop: -profileHeightRef.current / 2,
            marginHorizontal: 20,
          }}
          onLayout={e => {
            profileHeightRef.current = e.nativeEvent.layout.height;
          }}>
          <ProfileCardContainer />
        </View>
        <PreferenceList>
          <PreferenceListItem
            preferenceIconName="information-variant"
            preferenceTitle="About"
            preferenceDescription="Information about the application"
            onPress={() => {
              navigation.navigate('About');
            }}
          />
          <PreferenceListItem
            preferenceIconName="logout"
            preferenceTitle="Venue Code"
            preferenceDescription="Navigate back to start and set new venue code."
            onPress={clearSavedData}
          />
        </PreferenceList>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  profileContainer: {
    flex: 1,
  },
  profileHeader: {
    alignItems: 'center',
  },
  profileImageContainer: {position: 'relative'},
  username: {
    fontSize: baseFontSize,
    fontWeight: 'bold',
    marginVertical: 15,
    color: darkGray,
  },
  text: {
    color: '#101010',
    fontSize: 24,
    fontWeight: 'bold',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {fontSize: 14, fontWeight: 'bold'},
  subtitle: {fontSize: 14, marginVertical: 10},
});
