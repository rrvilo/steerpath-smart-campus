/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React, {useState, useEffect, useRef} from 'react';
import {StyleSheet, View, Text, Keyboard, Platform} from 'react-native';
import {colors} from '../styles/colors';
import {Image, Input, Button} from 'react-native-elements';
import {useSteerpathContext} from '../context/SteerpathContext';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Spinner from '../components/Spinner';
import ColoredStatusBar from '../components/status-bar/ColoredStatusBar';

export default function SetCodeScreen({navigation}) {
  const inputRef = useRef<any>(null);
  const [code, setCode] = useState<string>('');
  const {
    errorMsg,
    setErrorMsg,
    fetchVenueData,
    isLoading,
  } = useSteerpathContext();

  const handleButtonClick = () => {
    Keyboard.dismiss();
    if (code.length > 0) {
      fetchVenueData(code);
    } else {
      // show error
    }
  };

  return (
    <>
      <ColoredStatusBar />
      <KeyboardAwareScrollView
        contentContainerStyle={styles.container}
        bounces={false}
        keyboardShouldPersistTaps={'handled'}>
        <Image
          source={require('../../assets/images/app_logo.png')}
          style={styles.image}
        />
        {/* <Text style={styles.title}>Welcome!</Text> */}
        <View style={styles.inputBackground}>
          <Text style={styles.title}>Venue code</Text>
          <Input
            ref={inputRef}
            inputContainerStyle={styles.inputField}
            placeholderTextColor={colors.white}
            inputStyle={styles.input}
            onChangeText={text => setCode(text)}
            value={code}
            errorStyle={{color: colors.error, fontSize: 15}}
            errorMessage={errorMsg}
            onFocus={() => {
              setErrorMsg('');
            }}
          />
          <View style={styles.buttonContainer}>
            {!isLoading ? (
              <Button
                onPress={() => {
                  handleButtonClick();
                }}
                title="Open venue"
                titleStyle={styles.buttonTitle}
                buttonStyle={styles.button}
              />
            ) : (
              <Spinner />
            )}
          </View>
        </View>

        <Text style={{color: colors.white, textAlign: 'center'}}>
          To use Steerpath's indoor positioning and wayfinding, please enter
          your venue's code. You can try Steerpath's office by using code{' '}
          <Text
            style={styles.codeLink}
            onPress={() => {
              setCode('QX3GLM');
            }}>
            QX3GLM
          </Text>
        </Text>
      </KeyboardAwareScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.brand_color,
    justifyContent: 'flex-start',
    padding: 20,
    alignItems: 'center',
  },
  image: {
    width: 150,
    height: 150,
  },
  title: {
    fontSize: 24,
    color: colors.white,
  },
  inputBackground: {
    alignSelf: 'stretch',
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 10,
    backgroundColor: colors.inputBackground,
    borderRadius: 10,
    padding: 10,
  },
  inputField: {
    marginTop: 15,
    borderBottomWidth: 1,
    borderBottomColor: colors.white,
  },
  input: {
    fontSize: 20,
    color: colors.white,
    textAlign: 'center',
  },
  buttonContainer: {
    marginTop: 30,
    alignSelf: 'stretch',
  },
  button: {
    backgroundColor: colors.white,
    alignSelf: 'center',
  },
  buttonTitle: {
    color: colors.black,
    fontFamily: 'Roboto-Regular',
  },
  codeLink: {
    fontWeight: 'bold',
    textDecorationLine: 'underline',
  },
});
