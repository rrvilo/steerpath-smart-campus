/*
 * Copyright 2020 Steerpath Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as Colors from './colors';

export const extraLargeFontSize = 22;
export const buttonFontSize = 14;

// base app font sizes
export const largeFontSize = 17;
export const baseFontSize = 14;
export const mediumFontSize = 16;
export const smallFontSize = 12;
export const smallestFontSize = 10;

export const largeHeaderFontSize = 17;
export const headerFontSize = 18;

const base = {
  alignItems: 'center',
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
};

export const link = {
  color: Colors.thoughtbotRed,
  fontWeight: 'bold',
};

export const bodyText = {
  color: Colors.baseText,
  fontSize: smallFontSize,
  lineHeight: 19,
};

export const headerText = {
  color: Colors.darkText,
  fontSize: headerFontSize,
  fontWeight: 'bold',
};

export const descriptionText = {
  color: Colors.baseText,
  fontSize: smallFontSize,
};

export const screenHeader = {
  ...base,
  color: Colors.baseText,
  fontSize: largeFontSize,
  fontWeight: 'bold',
};

export const screenFooter = {
  ...base,
  ...descriptionText,
};

export const sectionHeader = {
  ...base,
  ...headerText,
};

export const count = {
  ...base,
  ...descriptionText,
};

// Face list typography

export const typography = {
  default: {
    fontFamily: 'Roboto-Regular',
    fontStyle: 'normal',
    fontWeight: 'normal',
  },
  h1: {
    fontSize: 22,
    lineHeight: 22,
  },
  h2: {
    fontSize: 17,
    lineHeight: 20,
  },
  h3: {
    fontSize: 16,
    lineHeight: 19,
  },
  h6: {
    fontSize: 12,
    lineHeight: 14,
  },
  body1: {
    fontSize: 15,
    lineHeight: 16,
    fontWeight: '300',
    fontFamily: 'Roboto-Light',
  },
  body2: {
    fontSize: 15,
    lineHeight: 16,
  },
  button: {
    fontSize: 15,
    lineHeight: 16,
  },
  tiny: {
    fontSize: 10,
    lineHeight: 12,
  },
};
